set_config:
	stow -d . -t ~

gen_themes:
	sudo touch /bedrock/strata/pop/usr/share/nvim/runtime/colors/base.vim
	sudo chown jabbi:jabbi /bedrock/strata/pop/usr/share/nvim/runtime/colors/base.vim
	sudo echo -en '" %%base16_template: vim##default %%\n\n"%%base16_template_end%%' > /bedrock/strata/pop/usr/share/nvim/runtime/colors/base.vim
	pybase16 inject \
		-s harmonic-dark\
		-f ~/.dotfiles/i3/.i3/config\
		-f ~/.dotfiles/alacritty/.config/alacritty/alacritty.yml\
		-f ~/.dotfiles/zathura/.config/zathura/zathurarc\
		-f ~/.dotfiles/tmux/.tmux.conf\
		-f ~/.dotfiles/rofi/.config/rofi/config.rasi\
		-f /bedrock/strata/pop/usr/share/nvim/runtime/colors/base.vim
	grep -v "syntax reset" /bedrock/strata/pop/usr/share/nvim/runtime/colors/base.vim > /tmp/fooooooooo
	sudo mv /tmp/fooooooooo /bedrock/strata/pop/usr/share/nvim/runtime/colors/base.vim
