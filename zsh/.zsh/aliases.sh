source /home/jabbi/git/uav_core/miscellaneous/shell_additions/shell_additions.sh

# File Explorer
#
alias l="exa"
alias ll="exa -l"

# todo
alias t="todo.sh"
alias tl="todo.sh ls"

# notes

alias nopen="notes list | fzf | xargs -o $EDITOR"

export ANDROID_SDK_ROOT="/home/jabbi/Android/Sdk"
export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-amd64"
#export JAVA_HOME="/usr/lib/jvm/java-1.17.0-openjdk-amd64"
export ANDROID_HOME=$ANDROID_SDK_ROOT
alias  hlpush="JAVA_HOME=\"/usr/lib/jvm/java-1.8.0-openjdk-amd64\" ionic cordova emulate android  -l --lc --port 4200 --device"
alias  hldb="/home/jabbi/Android/Sdk/platform-tools/adb -d shell 'run-as com.forstify.dev cat ./databases/database.db'"


# Go stuff
export GOPATH=$(go env GOPATH)
export PATH=$PATH:$(go env GOPATH)/bin
# SSH Socket

export VISUAL="nvim"
export EDITOR="$VISUAL"

# source some secrets
# source ~/.secrets

export NOTES_CLI_HOME=~/notes

# MRS uav_core shell configuration
# source /home/jabbi/git/uav_core/miscellaneous/shell_additions/shell_additions.sh
export UAV_NAME="uav1" 
export NATO_NAME="" # lower-case name of the UAV frame {alpha, bravo, charlie, ...}
export UAV_MASS="3.0" # [kg], used only with real UAV
export RUN_TYPE="simulation" # {simulation, uav}
export UAV_TYPE="f550" # {f550, f450, t650, eagle, naki}
export PROPULSION_TYPE="default" # {default, new_esc, ...}
export ODOMETRY_TYPE="gps" # {gps, optflow, hector, vio, ...}
export INITIAL_DISTURBANCE_X="0.0" # [N], external disturbance in the body frame
export INITIAL_DISTURBANCE_Y="0.0" # [N], external disturbance in the body frame
export STANDALONE="false" # disables the core nodelete manager
export SWAP_GARMINS="false" # swap up/down garmins
export PIXGARM="false" # true if Garmin lidar is connected throught Pixhawk
export SENSORS="" # {garmin_down, garmin_up, rplidar, realsense_front, teraranger, bluefox_optflow, realsense_brick, bluefox_brick}
export WORLD_NAME="simulation" # e.g.: "simulation" <= mrs_general/config/world_simulation.yaml
export MRS_STATUS="readme" # {readme, dynamics, balloon, avoidance, control_error, gripper}
export LOGGER_DEBUG="false" # sets the ros console output level to debug

source /usr/share/gazebo/setup.sh

source /home/jabbi/mrs_workspace/devel/setup.zsh

source /home/jabbi/Projects/robots/learning/myworkspace/devel/setup.zsh

export ROS_WORKSPACES="~/mrs_workspace ~/workspace"

alias swag="strat arch swag"
